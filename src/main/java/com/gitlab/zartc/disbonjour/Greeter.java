package com.gitlab.zartc.disbonjour;

import java.util.Random;


public class Greeter {
    private static final Random rnd = new Random();

    String[] salutations = new String[] {
            "how are you?",
            "nice to meet you.",
            "welcome to my humble place.",
            "did you have a good journey?",
            "how's the life goes on lately?",
            "did you meet my brother yet?"
    };

    public String greet(String name) {
        return " > Hello " + name + ", " + salutations[rnd.nextInt(salutations.length)];
    }
}

/* EOF */
