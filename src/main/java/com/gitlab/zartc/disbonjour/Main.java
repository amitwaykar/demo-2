package com.gitlab.zartc.disbonjour;

import java.util.Arrays;
import java.util.Scanner;


public class Main {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        var greeter = new Greeter();
        var name = "";

        System.out.println("welcome to Alzheimer planet...");

        while (true) {
            System.out.println("\nwhat's your name again ? ");
            name = scanner.nextLine();

            if (wantsStop(name)) {
                return;
            }

            System.out.println(greeter.greet(name));
        }
    }

    public static boolean wantsStop(String input) {
        return (Arrays.asList("stop", "quit", "by").contains(input));
    }
}

/* EOF */
